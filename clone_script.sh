#!/bin/bash

REPO=${REPO:-raghav-rama/aiml-lab}
REMOTE=${REMOTE:-https://gitlab.com/${REPO}.git}
BRANCH=${BRANCH:-master}
DIRNAME=${DIRNAME:-~/aiml-lab}

clone() {
    mkdir "$DIRNAME"
    git init --quiet "$DIRNAME" && cd "$DIRNAME" \
    && git config core.eol lf \
    && git config core.autocrlf false \
    && git config fsck.zeroPaddedFilemode ignore \
    && git config fetch.fsck.zeroPaddedFilemode ignore \
    && git config receive.fsck.zeroPaddedFilemode ignore \
    && git config oh-my-zsh.remote origin \
    && git config oh-my-zsh.branch "$BRANCH" \
    && git remote add origin "$REMOTE" \
    && git fetch --quiet --depth=1 origin \
    && git checkout --quiet -b "$BRANCH" "origin/$BRANCH" || {
        [ ! -d "$DIRNAME" ] || {
        cd -
        rm -rf "$DIRNAME" 2>/dev/null
        }
        exit 1
    }
    if [ "$#" -ne 1 ]; then
        echo "Unknown program, cloning the entire repo at $DIRNAME"
        cd $DIRNAME
        exit 1
    fi
    cat $1.py > ~/$1.py
    echo "$1.py copied to home directory" 
    cd ../
    rm -rf "$DIRNAME" 2>/dev/null
}

while [ $# -gt 0 ]; do
    case $1 in
        -p1) clone "p1" ;;
        -p2) clone "p2" ;;
        -p3) clone "p3" ;;
        -p4) clone "p4" ;;
        -p5) clone "p5" ;;
        -p6) clone "p6" ;;
        *) clone ;;
    esac
    shift
done
